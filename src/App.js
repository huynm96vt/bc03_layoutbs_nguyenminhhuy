import logo from './logo.svg';
import './App.css';
import BTLayoutBS from './BTLayoutBS/BTLayoutBS';

function App() {
  return (
    <div className="App">
      <BTLayoutBS/>
    </div>
  );
}

export default App;
